<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function (Request $request) {return response()->json(['error' => 'Not found'], 400);})->name('login');

Route::group(['prefix' => 'v1'], function () {
    Route::get('/no-auth', function (Request $request) {return response()->json(['error' => 'Unauthorized'], 401);})->name('login');

    Route::group(['middleware' => ['api', 'guest']], function () {
        Route::group(['as' => 'auth.'], function () {
            Route::post('api.auth.reg', 'AuthController@register');
            Route::post('api.auth.enter', 'AuthController@login');
            Route::post('api.auth.social', 'AuthController@social');
            Route::get('api.auth.city.like', 'AuthController@cityLike');
            Route::get('api.auth.verify', 'AuthController@verify')->name('verify');
            Route::post('api.auth.restore', 'AuthController@restore');
            Route::post('api.auth.recovery', 'AuthController@recovery');
            Route::post('api.auth.resend.verify', 'AuthController@resendVerifyEmail');
            Route::get('api.auth.check.email', 'AuthController@checkEmail');
        });

        Route::group(['as' => 'user.'], function () {
            Route::post('api.auth.exit', 'AuthController@logout');
            Route::post('api.auth.refresh', 'AuthController@refresh');
            Route::get('api.auth.user', 'AuthController@userProfile');

            Route::post('api.user.profile.remove', 'UsersController@removeProfile');
            Route::post('api.user.profile.save', 'UsersController@saveProfile');
            Route::post('api.user.profile.upload.avatar', 'UsersController@uploadAvatar');
            Route::post('api.user.profile.remove.avatar', 'UsersController@removeAvatar');
            Route::post('api.user.profile.change.email', 'AuthController@changeEmail');
            Route::get('api.user.profile.change.email.verify', 'AuthController@changeEmailVerify')->name('email.change');
            Route::post('api.user.blacklist.add', 'UsersController@blacklistAdd');
            Route::post('api.user.blacklist.remove', 'UsersController@blacklistRemove');
            

            Route::post('api.friends.add', 'FriendsController@add');
            Route::post('api.friends.process', 'FriendsController@process');
            Route::post('api.friends.remove', 'FriendsController@remove');

            Route::post('api.pet.profile.create', 'PetsController@createProfile');
            Route::post('api.pet.profile.save', 'PetsController@saveProfile');
            Route::post('api.pet.profile.remove', 'PetsController@removeProfile');
            Route::post('api.pet.profile.remove.avatar', 'PetsController@removeAvatar');
            Route::post('api.pet.profile.rip', 'PetsController@ripProfile');
            Route::get('api.pet.parents.autocomlete.owner', 'PetsController@autocomleteOwner');
            Route::get('api.pet.parents.autocomlete.pet', 'PetsController@autocomletePet');
            Route::post('api.pet.parents.send', 'PetsController@parentSend');
            Route::post('api.pet.parents.request', 'PetsController@parentRequest');
            Route::get('api.pet.parents.offsprings.{petId}', 'PetsController@getOffspring');
            Route::get('api.pet.parents.{petId}', 'PetsController@getParents');
            Route::post('api.pet.achievement.create', 'PetsController@createAchievement');
            Route::post('api.pet.achievement.save', 'PetsController@saveAchievement');
            Route::post('api.pet.achievement.remove', 'PetsController@removeAchievement');
            Route::get('api.pet.achievements.{petId}', 'PetsController@getAchievement');
            Route::get('api.pets.user.{userId}', 'UsersController@getPetsUser');
            Route::get('api.pet.profile.{petId}', 'PetsController@getPetProfile');

            Route::get('api.communities.autocomplete', 'CommunitiesController@autocomplete');
            Route::get('api.communities.get', 'CommunitiesController@clubs');
            Route::get('api.communities.groups', 'CommunitiesController@groups');
            Route::get('api.communities.group.{thisid}', 'CommunitiesController@getOne');
            Route::post('api.communities.subscribe', 'CommunitiesController@subscribe');
            Route::post('api.communities.unsubscribe', 'CommunitiesController@unsubscribe');

            Route::get('api.user.profile.{thisid}', 'UsersController@getUserProfile');
            Route::get('api.user.friends.{thisid}', 'FriendsController@getUserFriends');
            Route::get('api.user.friendsMutual.{thisid}', 'FriendsController@getUserFriendsMutual');
            
            Route::post('api.message.send', 'MessagesController@sendMsg');
            Route::get('api.message.get.dialogs', 'MessagesController@getDialogs');
            Route::get('api.message.get.dialog.{thisis}', 'MessagesController@getDialog');

            Route::get('api.business.getDraft', 'BusinessController@getDraft');
            Route::post('api.business.create', 'BusinessController@create');
            Route::post('api.business.verify', 'BusinessController@verify')->name('business.verify');

            Route::post('api.publication.create', 'PostsController@create');
            Route::post('api.publication.edit', 'PostsController@edit');
            Route::post('api.publication.remove', 'PostsController@remove');
            Route::post('api.publication.like', 'PostsController@like');
            Route::get('api.publication.post.{id}', 'PostsController@getPost');
            Route::get('api.publications.{type}.{id}', 'PostsController@getPosts');
            Route::get('api.feed.get', 'PostsController@getFeed');
            Route::get('api.feed.get.test', 'PostsController@getFeedTest');

            Route::get('api.search.pets', 'PetsController@search');
            Route::get('api.search.users', 'UsersController@search');

            Route::post('api.сomments.create', 'PostsController@createComment');
            Route::post('api.сomments.remove', 'PostsController@removeComment');
            Route::get('api.сomments.get.{type}.{id}', 'PostsController@getComments');
            
            

        });
    });
});



// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
