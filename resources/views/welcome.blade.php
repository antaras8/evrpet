<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

                <div class="top-right links">

 
                        <div class="mb-0">
                        <a class="btn btn-block btn-social btn-facebook" onClick="socialSignin('facebook');">
                            <span class="fa fa-facebook"></span> Sign in with Facebook
                        </a><br>
                        <a class="btn btn-block btn-social btn-google"  onClick="socialSignin('google');">
                            <span class="fa fa-google"></span> Sign in with Google
                        </a><br>
                        <!-- <a class="btn btn-block btn-social btn-google"  onClick="socialSignin('apple');">
                            <span class="fa fa-google"></span> Sign in with Apple
                        </a> -->
                        </div>
                        <form id="social-login-form" action="" method="POST" style="display: none;">
                        <input id="social-login-access-token" name="social-login-access-token" type="text">
                        <input id="social-login-tokenId" name="social-login-tokenId" type="text">
                        </form>
                </div>
            

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>
                

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
        <script src="https://www.gstatic.com/firebasejs/7.14.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.0/firebase-auth.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
// Initialize Firebase
var config = {
   // This is the variable you got from Firebase's Firebase SDK snippet. It includes values for apiKey, authDomain, projectId, etc.
   apiKey: "AIzaSyDt1UPNyWJTMI8ACh0ODrltZg4lxIpbN6k",
  authDomain: "evr-test-c1eaa.firebaseapp.com",
  projectId: "evr-test-c1eaa",
  storageBucket: "evr-test-c1eaa.appspot.com",
  messagingSenderId: "109938821339",
  appId: "1:109938821339:web:bdfa6efd59bea76ab4e4fe"
};
firebase.initializeApp(config);
var facebookProvider = new firebase.auth.FacebookAuthProvider();
var googleProvider = new firebase.auth.GoogleAuthProvider();
// var appleProvider = new firebase.auth.OAuthProvider();
var facebookCallbackLink = '/v1/api.auth.callback.facebook';
var googleCallbackLink = '/v1/api.auth.callback.google';
// var applceCallbackLink = '/v1/api.auth.callback.apple';
async function socialSignin(provider) {
   var socialProvider = null;
   if (provider == "facebook") {
      socialProvider = facebookProvider;
      document.getElementById('social-login-form').action = facebookCallbackLink;
   } else if (provider == "google") {
      socialProvider = googleProvider;
      document.getElementById('social-login-form').action = googleCallbackLink;
//    } else if (provider == "apple") {
//       socialProvider = appleProvider;
//       document.getElementById('social-login-form').action = applceCallbackLink;
   } else {
      return;
   }
firebase.auth().signInWithPopup(socialProvider).then(function(result) {
      result.user.getIdToken().then(function(result) {
         document.getElementById('social-login-tokenId').value = result;
         document.getElementById('social-login-form').submit();
      });
   }).catch(function(error) {
      // do error handling
      console.log(error);
   });
}
</script>
    </body>
</html>
