<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'net_city';

    const UPDATED_AT = NULL;
    const CREATED_AT = NULL;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_ru',
        'name_en'
    ];

    public function country()
    {
        return $this->hasOne('App\Country', 'id', 'country_id')->select(['id', 'name_ru']);
    }
}
