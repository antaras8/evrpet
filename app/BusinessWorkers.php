<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessWorkers extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'business_id', 'name', 'desc', 'services', 'avatar'
    ];

    const UPDATED_AT = NULL;
    const CREATED_AT = NULL;
}