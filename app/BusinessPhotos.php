<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessPhotos extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'business_id', 'file'
    ];

    const UPDATED_AT = NULL;
    const CREATED_AT = NULL;
}