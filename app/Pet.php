<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Helpers\Age;
use App\Communitie;

class Pet extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner_id', 'type', 'breed_id', 'sex', 'name', 'title', 'birth_time', 'death_date', 'status', 'picture', 'ip_address'
    ];

    // status 1 - живой 2 - на радуге 3 - желает завести 4 - remove

    // sex 1 -male 0 -female

    const UPDATED_AT = NULL;
    const CREATED_AT = 'added';

    // public function owner()
    // {
    //     return $this->hasOne('App\User', 'id', 'owner_id')->select(['id', 'name', 'surname', 'picture', 'ID_City'])->with('city');
    // }
    
    static function format($pet, $type = null) {
        if (!$pet) return;

        if($type == 'short' || $type == 'parent' || $type == 'user_profile' || $type == 'pet_profile'){
            $shortItem = new Pet();
            
            $shortItem->thisid = $pet->id;
            $shortItem->age = Age::getAgeByBirthtime($pet->birth_time);
            $shortItem->avatar = $pet->picture;
            $shortItem->name = $pet->name;
            $shortItem->status = $pet->status;
            $shortItem->death_date = $pet->death_date;
            $shortItem->birth_time = $pet->birth_time;
            $shortItem->title = $pet->title;

            if($type == 'parent' || $type == 'pet_profile') {
                $owner = User::whereId($pet->owner_id)->first();
                $shortItem->owner = [
                    'thisid' => $owner->id,
                    'fullname' => "{$owner->name} {$owner->surname}",
                    'url' => ($owner->slug <> NULL) ? $owner->slug : "user{$owner->id}"
                ];

                if($type == 'pet_profile'){
                    $shortItem->owner = [
                        'thisid' => $owner->id,
                        'fullname' => "{$owner->name} {$owner->surname}",
                        'url' => ($owner->slug <> NULL) ? $owner->slug : "user{$owner->id}",
                        'avatar' => $owner->picture,
                        'city' => City::whereId($owner->ID_City)->value('name_ru'),
                        'reg_date' => $owner->reg_date
                    ];
                }
            }

            if($type == 'user_profile' || $type == 'pet_profile' || $type == 'parent'){
                $club = Communitie::whereId($pet->breed_id)->first();
                $shortItem->specie = $club->titleru;
                $shortItem->type = $club->group;
                $shortItem->communityUrl = "community{$pet->breed_id}";
                $shortItem->sex = $pet->sex;
            }

            return $shortItem;
        }

        
        $club = Communitie::whereId($pet->breed_id)->first();
        $pet->thisid = $pet->id;
        $pet->userid = $pet->owner_id;
        $pet->age = Age::getAgeByBirthtime($pet->birth_time);
        $pet->group = $pet->type;
        $pet->specie = $club->titleru;
        $pet->specie_group = $club->code;
        $pet->type = $club->group;
        // $pet->avatar = UploadController::getUrl("pets", "avatar", $pet->thisid);
        $pet->avatar = $pet->picture;
        $pet->communityUrl = "community{$pet->breed_id}";
        unset($pet->picture);
        unset($pet->id);
        unset($pet->type);
        unset($pet->owner_id);
        unset($pet->breed_id);
        unset($pet->ip_address);
        return $pet;
    }
}
