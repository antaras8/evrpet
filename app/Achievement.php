<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Achievement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'achievement', 'pet_id', 'year', 'event', 'ID_City'
    ];

    const UPDATED_AT = NULL;
    const CREATED_AT = NULL;

    public function city()
    {
        return $this->hasOne('App\City', 'id', 'ID_City')->select('name_ru', 'id');
    }

}