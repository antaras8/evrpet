<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'net_country';

    const UPDATED_AT = NULL;
    const CREATED_AT = NULL;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_ru',
        'name_en',
        'code'
    ];

}
