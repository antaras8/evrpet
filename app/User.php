<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
// JWT contract
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Blacklist;
use App\City;
use App\Country;
use PDO;

class User extends Authenticatable implements JWTSubject {
    use Notifiable;

    const UPDATED_AT = NULL;
    const CREATED_AT = 'reg_date';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'surname',
        'reg_date',
        'slug',
        'phone',
        'ID_City',
        'title',
        'picture',
        'rating',
        'password',
        'session_token',
        'status',
        'birth_time',
        'uid',
        'online',
        'last_online'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'refresh_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime'
    ];
    
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }    

    public function city()
    {
        return $this->hasOne('App\City', 'id', 'ID_City')->select(['id', 'name_ru']);
    }

    static public function format($item, $type = null) {
        if (!$item) return;

        if($type == 'short' || $type == 'friends' || $type == 'profile'){
            $shortItem = new User();
            
            $shortItem->thisid = $item->id;
            $shortItem->fullname = "{$item->name} {$item->surname}";
            $shortItem->avatar = $item->picture;
            $shortItem->city = City::whereId($item->ID_City)->value('name_ru');

            if($type == 'friends') $shortItem->url = ($item->slug <> NULL) ? $item->slug : "user{$item->id}";
            if($type == 'profile'){
                $shortItem->reg_date = $item->reg_date;
                $shortItem->phone = $item->phone;
                $shortItem->title = $item->title;
                $shortItem->blacklist = (int)Blacklist::isBlacklist(auth()->user()->id, $shortItem->thisid, 'profile');
                $shortItem->birth_time = $item->birth_time;
            }

            return $shortItem;
        }

        $item->blacklist = (int)Blacklist::isBlacklist(auth()->user()->id, $item->thisid, 'profile');
        $city = City::whereId($item->ID_City)->first();
        $item->city = ($city) ? $city->name_ru : 'Город';
        $item->country = ($city) ? Country::whereId($city->country_id)->value('name_ru') : 'Страна';
        $item->thisid = $item->id;
        $item->fullname = "{$item->name} {$item->surname}";
        $item->url = ($item->slug <> NULL) ? $item->slug : "user{$item->thisid}";
        $item->avatar = $item->picture;
        unset($item->id);
        unset($item->picture);
        unset($item->ID_City);
        // unset($item->name);
        // unset($item->surname);
        unset($item->uid);
        unset($item->slug);
        unset($item->email);
        unset($item->email_verified_at);
        unset($item->status);
        // unset($item->birth_time);
        // unset($item->phone);
        unset($item->rating);
        unset($item->uid);

        return $item;
    }
}