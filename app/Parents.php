<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parents extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pet_id', 'parent_id', 'type', 'state'
    ];

    // type 1 - papa; 2 - mama
    // state 0 - wait, 1 - accept , 2 - canceled

    const UPDATED_AT = NULL;
    const CREATED_AT = NULL;

    public function pet()
    {
        return $this->hasOne('App\Pet', 'id', 'parent_id');
    }

    public function offsprings()
    {
        return $this->hasOne('App\Pet', 'id', 'pet_id');
    }

}
