<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Businesses extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner_id', 'step', 'draft', 'name', 'desc', 'logo', 'house', 'office', 'address', 'address_desc', 'phone', 'email', 'site', 'works', 'video', 'workers', 'loyalty_program', 'state', 'workes'
    ];

    /*
    STATE
    0 - создан
    1 - отправлен на модерацию (без потверждения по почте)
    2 - на модерации

    Draft
    0 - не черновик
    1 - черновик

    */

    const UPDATED_AT = NULL;

    public function photos()
    {
        return $this->hasMany('App\BusinessPhotos', 'business_id', 'id');
    }

    public function services()
    {
        return $this->hasMany('App\BusinessServices', 'business_id', 'id');
    }

    public function listWorkers()
    {
        return $this->hasMany('App\BusinessWorkers', 'business_id', 'id');
    }
    
}