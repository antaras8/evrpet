<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     * 1 - пользователи
     * 2 - клубы
     * 3 - бизнесы
     * 4 - питомцы
     * 
     * draft
     * 1 - черновик
     * 0 - не черновик
     */
    protected $fillable = [
        'author_id', 'type', 'header', 'body', 'tags', 'draft', 'preview', 'object_id'
    ];

    const UPDATED_AT = NULL;
    
    public function author()
    {
        return $this->hasOne('App\User', 'id', 'author_id')->select(['id', 'name', 'surname', 'picture']);
    }

    public function likes()
    {
        return $this->hasMany('App\Likes', 'post_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comments', 'object_id', 'id');
    }

}