<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */

    protected $fillable = [
        'author_id', 'object_id', 'parents_id', 'text'
    ];

    const UPDATED_AT = NULL;
    
    public function author()
    {
        return $this->hasOne('App\User', 'id', 'author_id')->select(['id', 'name', 'surname', 'picture']);
    }

    public function comments()
    {
        return $this->hasMany('App\Comments', 'parents_id', 'id')->select(['id', 'parents_id']);
    }

    public function likes()
    {
        return $this->hasMany('App\Likes', 'post_id', 'id');
    }

}