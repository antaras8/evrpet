<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blacklist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user1id', 'user2id', 'type'
    ];

    const UPDATED_AT = NULL;
    const CREATED_AT = NULL;

    static public function isBlacklist($viewerid, $userId, $type) {
        if($type == 'message'){
            return Blacklist::whereType($type)->where(function($query) use($viewerid, $userId) {
                $query->where('user2id', $viewerid)
                      ->where('user1id', $userId);
            })->orWhere(function($query) use($viewerid, $userId) {
                $query->where('user1id', $viewerid)
                      ->where('user2id', $userId);
            })->exists();
        }else{
            return Blacklist::whereUser1id($viewerid)->whereUser2id($userId)->whereType($type)->exists();
        }
    }

    static public function inBlacklist($viewerid, $userId, $type) {
        if($type == 'message'){
            return Blacklist::whereType($type)->where(function($query) use($viewerid, $userId) {
                $query->where('user2id', $viewerid)
                      ->where('user1id', $userId);
            })->orWhere(function($query) use($viewerid, $userId) {
                $query->where('user1id', $viewerid)
                      ->where('user2id', $userId);
            })->exists();
        }else{
            return Blacklist::whereUser2id($viewerid)->whereUser1id($userId)->whereType($type)->exists();
        }
    }

}
