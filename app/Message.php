<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public $fillable = ['dialog_id', 'sender_id', 'receiver_id', 'text'];

    const UPDATED_AT = NULL;

    public function sender()
    {
        return $this->hasOne('App\User', 'id', 'sender_id')->select(['picture', 'name', 'surname', 'id']);
    }
}
