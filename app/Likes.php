<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Likes extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     * type
     * 0 - post
     * 1 - comment
     */
    protected $fillable = [
        'post_id', 'user_id', 'type'
    ];

    const UPDATED_AT = NULL;

}