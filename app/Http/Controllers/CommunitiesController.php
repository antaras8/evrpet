<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Communitie;
use App\Participation;

class CommunitiesController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function subscribe(Request $request)
    {
        $breed_id = $request->has('breed_id') ? $request->breed_id : 0;
        Participation::subscribe(auth()->user()->id, $breed_id);
        return response()->json(['success' => true, 'message' => 'User has been successfully subcribe']);
    }

    public function unsubscribe(Request $request)
    {
        $breed_id = $request->has('breed_id') ? $request->breed_id : 0;
        Participation::unsubscribe(auth()->user()->id, $breed_id);
        return response()->json(['success' => true, 'message' => 'User has been successfully unsubcribe']);
    }

    public function clubs(Request $request)
    {
        $code = $request->has('code') ? $request->code : 'dog';
        
        $items = Communitie::whereGroup($code);

        if($request->has('name')) $items = $items->where('titleru', 'like', $request->name.'%');

        $items = $items->withCount('participation')->orderBy('participation_count', 'desc')->paginate(12);

        foreach ($items as $index => $val) {
            $val = Communitie::format($val);
            // $val->text = $val->title;
        }

        return response()->json(['clubs' => $items]);
    }
    
    public function groups()
    {
        $groups = Communitie::whereGroup(NULL)->whereNotNull('code')->select('id', 'titleru', 'code')->get();
        
        return response()->json(['groups' => $groups]);
    }
    
    public function getOne($thisid)
    {
        $item = Communitie::whereId($thisid)->withCount('participation')->first();
        return response()->json([
            'club' => Communitie::format($item),
            'subscription' => ($item) ? Participation::whereGroupId($item->id)->whereUserId(auth()->user()->id)->exists() : false
        ]);

    }
        
    public function autocomplete(Request $request)
    {
        $group = $request->group ? $request->group : '';
        $title = $request->title ? $request->title : '';
        $items = Communitie::whereGroup($group)->where('titleru', 'LIKE', $title."%")->withCount('participation')->orderBy('participation_count', 'desc')->paginate(20);

        foreach ($items as $index => $val) {
            $val = Communitie::format($val);
            // $val->text = $val->title;
        }

        return response()->json(['clubs' => $items]);
    }
}
