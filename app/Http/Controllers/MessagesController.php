<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dialog;
use App\Message;
use App\User;
use App\Blacklist;
use Validator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

class MessagesController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function sendMsg(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'text' => 'required|string|between:1,1024',
            'receiver_id' => 'required|numeric'
        ]);

        if(!User::whereId($request->receiver_id)->exists()) $validator->errors()->add("receiver_id", "User not found.");

        if (Blacklist::isBlacklist($request->receiver_id, auth()->user()->id, 'message')) $validator->errors()->add("receiver_id", "User in blacklist.");

        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }

        if(auth()->user()->id <> $request->receiver_id){
            $dialog = Dialog::where(function($query) use($request) {
                $query->where('second_id', $request->receiver_id)
                      ->where('first_id', auth()->user()->id);
            })->orWhere(function($query) use($request) {
                $query->where('first_id', $request->receiver_id)
                      ->where('second_id', auth()->user()->id);
            })->first();

            if(!$dialog){
                $dialog = Dialog::create([
                    'first_id' => auth()->user()->id,
                    'second_id' => $request->receiver_id,
                    'last_sender_id' => auth()->user()->id
                ]);
            }

            $message = Message::create([
                'dialog_id' => $dialog->id,
                'sender_id' => auth()->user()->id,
                'receiver_id' => $request->receiver_id,
                'text' => $request->text
            ]);

            $dialog->update([
                'last_message_id' => $message->id,
                'last_sender_id' => auth()->user()->id,
                'unread' => 1
            ]);

            $message = [
                "id" => $message->id,
                "dialog_id" => $message->dialog_id,
                "receiver_id" => $message->receiver_id,
                "text" => $message->text,
                "date" => $message->created_at->format('G:i'),
                'sender' => [
                    'fullname' => auth()->user()->name." ".auth()->user()->surname,
                    'avatar' => auth()->user()->picture,
                    'id' => auth()->user()->id
                ]
            ];

            Redis::publish('messages', json_encode(['userId' => $request->receiver_id, 'msg' => $message]));

            return response()->json(['message' => 'Message successfully send', 'msg' => $message]);
        }
    }
    
    public function getDialogs()
    {
        Carbon::setLocale('ru');
        $dialogs = Dialog::where('first_id', auth()->user()->id)->orWhere('second_id', auth()->user()->id)->with('lastMessage')->paginate(20);
        foreach($dialogs as $dialog){
            if(isset($dialog->lastMessage)){
                $dialog->lastMessage->sender->fullname = "{$dialog->lastMessage->sender->name} {$dialog->lastMessage->sender->surname}";
                $dialog->lastMessage->sender->avatar = $dialog->lastMessage->sender->picture;
                $dialog->lastMessage->date = $dialog->lastMessage->created_at->diffForHumans();

                unset($dialog->lastMessage->id);
                unset($dialog->lastMessage->sender_id);
                unset($dialog->lastMessage->sender->picture);
                unset($dialog->lastMessage->sender->name);
                unset($dialog->lastMessage->sender->surname);
                unset($dialog->lastMessage->created_at);
            }
            unset($dialog->first_id);
            unset($dialog->second_id);
            unset($dialog->last_message_id);
            unset($dialog->last_sender_id);
        }
        return response()->json(['dialogs' => $dialogs]);
    }

    public function getDialog($dialogId)
    {
        $dialog = Dialog::whereId($dialogId)->first();
        if(!$dialog) return response()->json(['message' => 'Dialog not found'], 400);
        if($dialog->first_id <> auth()->user()->id && $dialog->second_id <> auth()->user()->id) return response()->json(['message' => 'Access denied'], 400);

        $messages = Message::whereDialogId($dialog->id)->with('sender')->paginate(20);
        foreach($messages as $message){
            if(isset($message->sender)){
                $message->sender->fullname = "{$message->sender->name} {$message->sender->surname}";
                $message->sender->avatar = $message->sender->picture;
                if($message->created_at >= Carbon::today()){
                    $message->date = $message->created_at->format('G:i');
                }elseif($message->created_at >= Carbon::yesterday()){
                    $message->date = $message->created_at->format('вчера, G:i');
                }else{
                    $message->date = $message->created_at->format('j.n.y, G:i');
                }

                unset($message->sender->picture);
                unset($message->sender->name);
                unset($message->sender->surname);
            }
            unset($message->created_at);
            // unset($message->id);
            unset($message->sender_id);
        }

        $dialog->update(['unread' => 0]);

        return response()->json(['messages' => $messages]);
    }
}
