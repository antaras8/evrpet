<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\Businesses;
use App\BusinessPhotos;
use App\BusinessServices;
use App\BusinessWorkers;
use Carbon\Carbon;

class BusinessController extends Controller
{

    const SALT = 'YrpvUuA80LF2VVq';

    public function __construct() {
        $this->middleware('auth:api');
    }

    public function create(Request $request)
    {
        $step = $request->step;

        switch ($step) {
            case 1:
                $validation = [
                    'name' => 'required|string|between:2,32',
                    'desc' => 'string|between:2,300'
                ];
                break;

            case 2:
                $validation = [
                    'house' => 'boolean',
                    'office' => 'boolean',
                    'address' => 'required|string|between:2,150',
                    'address_desc' => 'string|between:2,300',
                    'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
                    'email' => 'email',
                    'site' => 'string|between:2,100'
                ];
                break;
            
            case 3:
                $validation = [
                    'works' => 'required|array'
                ];
                break;

            case 4:
                $validation = [
                    'video' => 'url'
                ];
                break;

            case 5:
                $validation = ['workes' => 'array'];
                break;
            
            case 6:
                $validation = [
                    'name' => 'string|between:2,32',
                    'desc' => 'string|between:2,300',
                    "price" => 'integer'
                ];
                break;

            case 7:
                $validation = [
                    'services' => 'array',
                    'name' => 'string|between:2,32',
                    'desc' => 'string|between:2,300',
                    'workers' => 'boolean'
                ];
                break;

            case 8:
                $validation = [
                    'loyalty_program' => 'string|between:1,300'
                ];
                break;

            case 9:
                $validation = [];
                break;

            default:
                return response()->json(['message' => 'Step not found']);
                break;
        }

        $validator = Validator::make($request->all(), $validation);
        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }

        $business = Businesses::whereOwnerId(auth()->user()->id)->whereDraft(1)->first();
        if(!$business) {
            if(Businesses::whereOwnerId(auth()->user()->id)->count() >= 1) return response()->json(['message' => 'Only 1 Business']);
            $business = Businesses::create([
                'owner_id' => auth()->user()->id,
                'draft' => 1
            ]);
        }

        if($step == 7 && $request->workers == 1){
            if(BusinessWorkers::whereBusinessId($business->id)->count() == 0) return response()->json(['message' => '0 workers']);
        }

        if($step >= $business->step){
            $business->update([
                'step' => $step+1
            ]);
        }

        switch ($step) {
            case 1:
                $logo = $request->logo;
                $logo = str_replace('data:image/png;base64,', '', $logo);
                $logo = str_replace(' ', '+', $logo);

                $logo = UploadController::save(base64_decode($logo), 'businesses/logos', "/storage/images/businesses/logos/no-photo.png"); 

                $business->update([
                    'name' => $request->name,
                    'desc' => $request->desc,
                    'logo' => $logo
                ]);

                break;

            case 2:
                $business->update([
                    'house' => $request->house,
                    'office' => $request->office,
                    'address' => $request->address,
                    'address_desc' => $request->address_desc,
                    'phone' => $request->phone,
                    'email' => $request->email,
                    'site' => $request->site,
                ]);
                break;

            case 3:
                $business->update([
                    'works' => $request->works
                ]);
                break;

            case 4:
                if($request->type == "add"){
                    if($request->class == 'video'){
                        $business->update([
                            'video' => $request->video
                        ]);
                    }else{
                        foreach ($request->photos as $photo) {
                            $image = $photo['image'];
                            $image = str_replace('data:image/png;base64,', '', $image);
                            $image = str_replace(' ', '+', $image);
    
                            $upload = UploadController::save(base64_decode($image), 'businesses/photos', NULL);
                            if($upload <> NULL) BusinessPhotos::create([
                                'business_id' => $business->id,
                                'file' => $upload
                            ]);
                        }
                    }
                }

                if($request->type == "remove"){
                    if($request->class == 'video'){
                        $business->update([
                            'video' => "NULL"
                        ]);
                    }else{
                        $photo = BusinessPhotos::whereBusinessId($business->id)->whereId($request->photo_id)->first();
                        if($photo){
                            unlink(storage_path('app/public').substr($photo->file, 8));
                            $photo->delete();
                        }
                    }
                }

                if(count($business->photos) < 4) return response()->json(['photos' => ['Minimum 4 photos']]);

                return response()->json(['success' => true, 'photos' => $business->photos, 'video' => $business->video]);
                break;

            case 5:
                $business->update([
                    'workes' => $request->works
                ]);
                break;

            case 6:
                if($request->type == "add") {
                    BusinessServices::create([
                        'business_id' => $business->id,
                        'name' => $request->name,
                        'desc' => $request->desc,
                        'price' => $request->price
                    ]);
                }

                if($request->type == "remove"){
                    $service = BusinessServices::whereBusinessId($business->id)->whereId($request->service_id)->first();
                    if($service) $service->delete();
                }

                if($request->type == "edit"){
                    $service = BusinessServices::whereBusinessId($business->id)->whereId($request->service_id)->first();
                    if($service) $service->update([
                        'name' => $request->name,
                        'desc' => $request->desc,
                        'price' => $request->price
                    ]);
                }

                return response()->json(['success' => true, 'services' => $business->services]);
                break;

            case 7:
                if($request->type == "add") {

                    $image = $request->image;
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);

                    $upload = UploadController::save(base64_decode($image), 'businesses/workers', NULL);
                    
                    if($upload <> NULL) BusinessWorkers::create([
                        'business_id' => $business->id,
                        'name' => $request->name,
                        'desc' => $request->desc,
                        'avatar' => $upload,
                        'services' => json_encode($request->services)
                    ]);

                    if(BusinessWorkers::whereBusinessId($business->id)->count() > 0) $business->update(['workers' => 1]);
                }

                if($request->type == "remove"){
                    $worker = BusinessWorkers::whereBusinessId($business->id)->whereId($request->worker_id)->first();
                    if($worker){
                        unlink(storage_path('app/public').substr($worker->avatar, 8));
                        $worker->delete();
                    }

                    if(BusinessWorkers::whereBusinessId($business->id)->count() == 0) $business->update(['workers' => 0]);
                }

                if($request->type == "edit"){
                    $worker = BusinessWorkers::whereBusinessId($business->id)->whereId($request->worker_id)->first();
                    if($worker){
                        if($request->has('image')){
                            $image = $request->image;
                            $image = str_replace('data:image/png;base64,', '', $image);
                            $image = str_replace(' ', '+', $image);

                            $upload = UploadController::save(base64_decode($image), 'businesses/workers', $worker->avatar);
                            if($upload <> $worker->avatar) {
                                unlink(storage_path('app/public').substr($worker->avatar, 8));
                                $worker->avatar = $upload;
                            }
                        }
                        $worker->update([
                            'name' => $request->name,
                            'desc' => $request->desc,
                            'avatar' => $worker->avatar,
                            'services' => json_encode($request->services)
                        ]);
                        
                    }
                }

                return response()->json(['success' => true, 'workers' => $business->listWorkers]);
                break;

            case 8:
                $business->update([
                    'loyalty_program' => $request->loyalty_program
                ]);
                break;

            case 9:
                $business->update([
                    'state' => 1,
                    'draft' => 0
                ]);

                $token = md5($business->id . $business->created_at . self::SALT);
                $link = route('user.business.verify').'?id='.$business->id.'&token='.$token;
                $email = auth()->user()->email;
                Mail::send(
                    'email.verify-businesses',
                    ['link' => $link],
                    function($message) use ($email) {
                        $message->to($email);
                        $message->subject('письмо с условиями использывания сервиса');
                    }
                );
                return response()->json(['success' => true, 'email' => $email, 'test-link' => $link]);
                break;

            default:
                return response()->json(['message' => 'Step not found']);
                break;
        }

        return response()->json(['success' => true]);
    }
    

    public function verify(Request $request)
    {
        $business = Businesses::whereState(1)->whereId($request->id)->first();
        if($business) {
            if (md5($business->id . $business->created_at . self::SALT) === $request->token) {
                $business->update([
                    'state' => 2
                ]);
                return response()->json(['message' => 'good verify']);
            }
        }

        return response()->json(['message' => 'bad verify']);
    }

    public function getDraft()
    {
        $business = Businesses::whereOwnerId(auth()->user()->id)->with(['photos', 'services', 'listWorkers'])->first();
        if(!$business) return response()->json(['message' => 'Business not found']);
        $business->owner_email = auth()->user()->email;

        return response()->json(['business' => $business]);
    }
}
