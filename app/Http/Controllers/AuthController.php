<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Firebase\Auth\Token\Verifier as Verifier;
use Carbon\Carbon;
use Validator;
use App\User;
use App\City;
use App\Country;

class AuthController extends Controller {

    const SALT = 'YrpvUuA80LF2VVq';
    /**
    * Create a new AuthController instance.
    *
    * @return void
    */

    public function __construct(Verifier $verifier) {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'social', 'cityLike', 'verify', 'restore', 'recovery', 'changeEmailVerify', 'resendVerifyEmail', 'refresh', 'checkEmail']]);
        $this->verifier = $verifier;
    }

    /**
    * Get a JWT via given credentials.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (! $token = auth()->attempt($validator->validated())) {
            if(User::whereEmail($request->email)->exists()){
                $validator->errors()->add("password", "Неверный пароль.");
            }else{
                $validator->errors()->add("email", "Такого аккаунта нет.");
            }
            
            return response()->json($validator->errors(), 422);
            // return response()->json(['error' => 'Email or password is not correct'], 422);
        }
        return $this->createNewToken($token);
    }

    /**
    * Get a JWT via given social.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function social(Request $request){
        $socialTokenId = $request->get('social-login-tokenId');
        
        try {
            $firebaseToken = $this->verifier->verifyIdToken($socialTokenId);

            $values = self::resolveByClaims($firebaseToken->getClaims());

            if(isset($values['email'])){
                $user = User::whereEmail($values['email'])->first();
            }else{
                $user = User::whereUid($values['user_id'])->first();
            }
 
            if (!$user) {
                $splitName = explode(' ', $values['name'], 2);
                $firstName = $splitName[0];
                $lastName = !empty($splitName[1]) ? $splitName[1] : ''; 

                $contents = file_get_contents($values['picture']);
                $picture = UploadController::save($contents, 'avatars', '/storage/images/avatars/no-photo.png'); 

                $user = User::create([
                    'name' => $firstName,
                    'surname' => $lastName,
                    'email' => isset($values['email']) ? $values['email'] : NULL,
                    'uid' => isset($values['email']) ? NULL : $values['user_id'],
                    'status' => 1,
                    'picture' => $picture
                ]);
                
            }

            if (! $token = auth()->login($user)) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
            return $this->createNewToken($token);

            // return dd($firebaseToken->getClaims());
        } catch (\Exception $e) {
            return dd($e);
            return response()->json(['error' => 'Firebase error'], 422);
        }
    }

    function resolveByClaims(array $claims)
    {
        $attributes = $this->transformClaims($claims);

        return $attributes;
    }

    public function transformClaims(array $claims): array
    {
        $attributes = [];

        if (!empty($claims['email'])) {
            $attributes['email'] = (string) $claims['email'];
        }

        if (!empty($claims['user_id'])) {
            $attributes['user_id'] = (string) $claims['user_id'];
        }

        if (!empty($claims['name'])) {
            $attributes['name'] = (string) $claims['name'];
        }

        if (!empty($claims['picture'])) {
            $attributes['picture'] = (string) $claims['picture'];
        }

        return $attributes;
    }


    /**
    * Register a User.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'city' => 'required|string|min:2',
        ]);

        $splitName = explode(' ', $request->name, 2);
        $firstName = $splitName[0];
        $lastName = !empty($splitName[1]) ? $splitName[1] : ''; 
        if($lastName == "") $validator->errors()->add("name", "Введите фамилию.");

        // dd(preg_match_all('/^[a-zа-йА-ЯёйЁЙьЬъЪыЫуУшШщЩцЦхХфФэЭжЖрРюЮчЧтТсСяЯ]+$/mi', $firstName), $firstName, preg_match_all('/^[a-zа-яА-ЯёйЁЙьъы]+$/mi', $lastName), $lastName);

        if (preg_match_all('/^[a-zа-йА-ЯёйЁЙьЬъЪыЫуУшШщЩцЦхХфФэЭжЖрРюЮчЧтТсСяЯ]+$/mi', $firstName) == 0) return response()->json(["name" => ["Имя может содержать только буквы."]], 400);
        if (preg_match_all('/^[a-zа-йА-ЯёйЁЙьЬъЪыЫуУшШщЩцЦхХфФэЭжЖрРюЮчЧтТсСяЯ]+$/mi', $lastName) == 0) return response()->json(["name" => ["Фамилия может содержать только буквы."]], 400);

        $city = explode(',', $request->city);
        if(count($city) == 1) {
            $validator->errors()->add("city", "Страна не введена");
        }else{
            $country = Country::whereNameRu(trim($city[1]))->first();
            if(!$country) $validator->errors()->add("city", "Страна не найдена");
            $cities = City::whereNameRu($city[0])->first();
            if(!$cities) $validator->errors()->add("city", "Город не найден.");
        }

        // $city = City::whereNameRu($request->city)->first();
        // if(!$city) $validator->errors()->add("city", "Enter valid city.");

        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }

        \DB::beginTransaction();
        
            $user = User::create([
                'email' => $request->email,
                'name' => $firstName,
                'surname' => $lastName,
                'password' => bcrypt($request->password),
                'phone' => $request->phone,
                'ID_City' => $cities->id,
                'status' => 0,
                'picture' => '/storage/images/avatars/no-photo.png'
            ]);

            $token = md5($user->email . $user->reg_date . self::SALT);
            $link = route('auth.verify').'?id='.$user->id.'&token='.$token;
            Mail::send(
                'email.verify-email',
                ['link' => $link],
                function($message) use ($request) {
                    $message->to($request->email);
                    $message->subject('Подтверждение адреса почты');
                }
            );

        \DB::commit();

        return response()->json([
            'message' => 'User successfully registered',
            'success' => true,
            // 'user' => User::format($user)
        ], 201);
    }

    public function resendVerifyEmail(Request $request)
    {
        $user = User::whereEmail($request->email)->first();
        if($user){
            if($user->status == 0){
                $token = md5($user->email . $user->reg_date . self::SALT);
                $link = route('auth.verify').'?id='.$user->id.'&token='.$token;
                Mail::send(
                    'email.verify-email',
                    ['link' => $link],
                    function($message) use ($request) {
                        $message->to($request->email);
                        $message->subject('Подтверждение адреса почты');
                    }
                );
                return response()->json(['message' => 'Email successfully send', 'test-link' => $link]);
            }
        }
        return response()->json(['message' => 'error'], 400);
        
    }
    /**
    * Log the user out (Invalidate the token).
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function logout() {
        auth()->logout();
        return response()->json(['message' => 'User successfully signed out']);
    }
    /**
    * Refresh a token.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function refresh(Request $request) {
        if($request->refresh_token){
            $user = User::whereRefreshToken($request->refresh_token)->first();
            if($user){
                $token = auth()->login($user);
                if($request->refresh_token == $user->refresh_token) return $this->createNewToken($token, $user->refresh_token, false);
            }
        }
        return response()->json(['message' => 'error'], 400);
    }
    /**
    * Get the authenticated User.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function userProfile() {
        return response()->json(User::format(auth()->user()));
    }
    /**
    * Get the token array structure.
    *
    * @param string $token
    *
    * @return \Illuminate\Http\JsonResponse
    */
    protected function createNewToken($token, $refresh = false, $user_view = true){
        if(auth()->user()->status == -1){
            auth()->logout();
            return response()->json(['email' => ['Аккаунт был удален.'] ], 401);
        }
        if(auth()->user()->status == 0){
            $email = auth()->user()->email;
            $token = md5($email . auth()->user()->reg_date . self::SALT);
            $link = route('auth.verify').'?id='.auth()->user()->id.'&token='.$token;
            Mail::send(
                'email.verify-email',
                ['link' => $link],
                function($message) use ($email) {
                    $message->to($email);
                    $message->subject('Подтверждение адреса почты');
                }
            );
            auth()->logout();
            return response()->json(['email' => ['Аккаунт не подтвержден. Письмо было отправлено повторно на Ваш email.']], 401);
        }
        if($refresh == false){
            $refresh = auth()->user()->refresh_token;
            if(!$refresh){
                $refresh = sha1(mt_rand(1, 90000) . self::SALT);
                User::whereId(auth()->user()->id)->update(['refresh_token' => $refresh]);
            }
        }
        
        return response()->json([
            'access_token' => $token,
            'refresh_token' => $refresh,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => ($user_view) ? auth()->user() : null
        ]);
    }

    public function checkEmail(Request $request)
    {
        $exists = User::whereEmail($request->email)->exists();
        return response()->json(['exists' => $exists]);
    }

    public function cityLike(Request $request)
    {
        $cities = [];
        $cityes = City::where('name_ru', 'LIKE', $request->city."%")->limit(20)->with('country')->select('country_id','name_ru')->get();
        foreach ($cityes as $city) {
            $cities[] = $city->name_ru.', '.$city->country->name_ru;
        }
        return response()->json($cities);
    }

    // public function cityLike(Request $request)
    // {
    //     $new = [];
    //     // $cityes = City::where('name_ru', 'LIKE', $request->city."%")->with('country')->limit(10)->get();
    //     $cityes = City::with('country')->select('id', 'country_id', 'name_ru')->get();
    //     foreach($cityes as $city){
    //         $new[] = ['text' => $city->name_ru.', '.$city->country->name_ru];
    //     }
    //     return response()->json($new);
    // }

    public function verify(Request $request) {
        $user = User::find($request->id);
        if($user && $user->status == 0){
            if (md5($user->email . $user->reg_date . self::SALT) === $request->token) {
                $user->update(['email_verified_at' => Carbon::now(), 'status' => 1]);
                // return response()->json(['message' => 'good verify']);
            }
        }
        return redirect()->away(env('APP_BASE_DOMEN'));
        // return response()->json(['message' => 'bad verify']);
    }

    public function restore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        $user = User::whereEmail($request->email)->first();
        if (!$user) $validator->errors()->add("email", "Email not found.");        

        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }

        \DB::beginTransaction();

            if($user->status == 0){
                $token = md5($user->email . $user->reg_date . self::SALT);
                $link = route('auth.verify').'?id='.$user->id.'&token='.$token;
                Mail::send(
                    'email.verify-email',
                    ['link' => $link],
                    function($message) use ($request) {
                        $message->to($request->email);
                        $message->subject('Подтверждение адреса почты');
                    }
                );
            }else{
                $check = \DB::table('password_resets')->whereEmail($request->email)->whereStatus(0)->first();
                if($check){
                    $token = $check->token;
                }else{
                    $token = Str::random(40);
                    \DB::table('password_resets')->insert([
                        'email' => $user->email,
                        'token' => $token,
                        'created_at' => Carbon::now(),
                        'ip' => $request->ip()
                    ]);
                }
    
                Mail::send(
                    'email.restore-email',
                    ['token' => $token],
                    function($message) use ($user) {
                        $message->to($user->email);
                        $message->subject('Восстановление пароля');
                    }
                );
            }

        \DB::commit();
        if($user->status == 0){
            return response()->json(['message' => 'Email successfully send', 'test-link' => $link]);
        }else{
            return response()->json(['message' => 'Email successfully send', 'token' => $token]);
        }
        
    }

    public function recovery(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required|string',
            'password' => 'required|string|min:6'
        ]);

        $user = \DB::table('password_resets')->whereToken($request->token)->first();
        if (!$user){
            $validator->errors()->add("token", "Token not found.");
        } else{
            if (time() - strtotime($user->created_at) > 600) $validator->errors()->add("token", "Token expired.");
            if ($user->status == 1) $validator->errors()->add("token", "Token used.");
        }

        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }
        
        User::whereEmail($user->email)->update(['password' => bcrypt($request->password)]);
        \DB::table('password_resets')->whereId($user->id)->update(['status' => 1]);
        return response()->json(['message' => 'Password successfully changed']);
    }

    public function changeEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:100|unique:users',
        ]);

        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }

        \DB::beginTransaction();

            $check = \DB::table('email_changes')->whereUserId(auth()->user()->id)->whereEmail($request->email)->whereStatus(0)->first();
            if($check){
                $token = $check->token;
            }else{
                $token = Str::random(40);
                \DB::table('email_changes')->insert([
                    'email' => $request->email,
                    'token' => $token,
                    'user_id' => auth()->user()->id,
                    'created_at' => Carbon::now(),
                ]);
            }

            $link = route('user.email.change').'?token='.$token;
            Mail::send(
                'email.change-email',
                ['link' => $link],
                function($message) use ($request) {
                    $message->to($request->email);
                    $message->subject('Смена адреса почты');
                }
            );

        \DB::commit();

        return response()->json(['message' => 'Email successfully send', 'test-link' => $link]);
    }

    public function changeEmailVerify(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required|string'
        ]);

        $user = \DB::table('email_changes')->whereToken($request->token)->first();
        if (!$user){
            $validator->errors()->add("token", "Token not found.");
        } else{
            if (time() - strtotime($user->created_at) > 600) $validator->errors()->add("token", "Token expired.");
            if ($user->status == 1) $validator->errors()->add("token", "Token used.");
        }

        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }
        
        User::whereId($user->user_id)->update(['email' => $user->email]);
        \DB::table('email_changes')->whereId($user->id)->update(['status' => 1]);
        return response()->json(['message' => 'Email successfully changed']);
    }
}