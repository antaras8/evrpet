<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Pet;
use App\Communitie;
use App\Participation;
use App\User;
use App\Parents;
use App\Achievement;
use App\City;
use App\Country;
use Carbon\Carbon;
use App\Helpers\DateOperations;

class PetsController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function saveProfile(Request $request) {
        $validator = Validator::make($request->all(), [
            'petId' => 'required',
            'name' => 'required|string|between:2,100',
            'title' => 'nullable|string|between:1,255',
            'specie' => 'required|string'
        ]);

        $pet = Pet::whereId($request->petId)->first();
        if (!$pet) {
            $validator->errors()->add("petId", "Pet not found");
            return response()->json($validator->errors(), 400);
        }
        if ($pet->owner_id <> auth()->user()->id) $validator->errors()->add("petId", "No editing permissions");

        $specie = Communitie::whereTitleru($request->specie)->first();
        if (!$specie) $validator->errors()->add("specie", "Enter valid specie.");

        if($request->has('birth_time')){
            $date = Carbon::parse($request->birth_time);
            $minDate = Carbon::createFromDate(1900, 1, 1);
            if (DateOperations::minDate($date, $minDate)) $validator->errors()->add("specie", "Minimum date 01.01.1900.");
            $maxDate = Carbon::today()->addDay();
            if (DateOperations::maxDate($date, $maxDate)) $validator->errors()->add("specie", "Maximum date is today.");
        }

        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }

        $avatar = $request->avatar;
        $avatar = str_replace('data:image/png;base64,', '', $avatar);
        $avatar = str_replace(' ', '+', $avatar);

        $picture = UploadController::save(base64_decode($avatar), 'pets/avatars', $pet->picture); 

        $pet->update([
            'name' => $request->name,
            'title' => $request->title,
            'breed_id' => $specie->id,
            'birth_time' => ($request->has('birth_time')) ? $date : $pet->birth_time,
            'picture' => $picture
        ]);
        return response()->json(['success' => true, 'message' => 'Pet successfully edited', 'pet' => Pet::format($pet)]);
    }
    
    public function createProfile(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'sex' => 'required|boolean',
            'specie' => 'required|string',
            'state' => 'required|numeric|between:1,3'
        ]);

        $specie = Communitie::whereTitleru($request->specie)->first();
        if (!$specie) $validator->errors()->add("specie", "Порода не найдена.");

        if($request->state == 1){
            if($request->has('birth_time')){
                if($request->birth_time == null) $validator->errors()->add("birth_time", "Введите дату рождения питомца.");
                $date = Carbon::parse($request->birth_time);
                $minDate = Carbon::createFromDate(1900, 1, 1);
                if (DateOperations::minDate($date, $minDate)) $validator->errors()->add("birth_time", "Минимальная дата 01.01.1900.");
                $maxDate = Carbon::today()->addDay();
                if (DateOperations::maxDate($date, $maxDate)) $validator->errors()->add("birth_time", "Максимальная дата это сегодня.");
            }else{
                $validator->errors()->add("birth_time", "Введите дату рождения питомца.");
            }
        }

        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }

        $avatar = $request->avatar;
        $avatar = str_replace('data:image/png;base64,', '', $avatar);
        $avatar = str_replace(' ', '+', $avatar);

        $picture = UploadController::save(base64_decode($avatar), 'pets/avatars', "/storage/images/pets/default/pet-{$specie->group}.svg"); 

        $pet = Pet::create([
            'name' => $request->name,
            'breed_id' => $specie->id,
            'sex' => $request->sex,
            'owner_id' => auth()->user()->id,
            'picture' => $picture,
            'type' => $specie->group,
            'ip_address' => $request->ip(),
            'status' => $request->state,
            'birth_time' => ($request->state == 1) ? $date : null
        ]);

        Participation::subscribe($pet->owner_id, $pet->breed_id);

        return response()->json(['success' => true, 'message' => 'Pet successfully created', 'pet' => Pet::format($pet)]);
    }

    public function removeAvatar(Request $request)
    {
        $pet = Pet::whereId($request->petId)->first();
        if (!$pet) return response()->json(['message' => "Pet not found"], 400);
        if ($pet->owner_id <> auth()->user()->id) return response()->json(['message' => "No editing permissions"], 400);

        $pet->update([
            'picture' => "/storage/images/pets/default/pet-{$pet->type}.svg"
        ]);

        return response()->json(['success' => true, 'message' => 'Avatar removed', 'avatar' => "/storage/images/pets/default/pet-{$pet->type}.svg"]);
    }
    
    public function removeProfile(Request $request) {
        if(!$request->has('petId')) return response()->json(['message' => 'Pet not found'], 400);

        $pet = Pet::whereId($request->petId)->first();
        if (!$pet) return response()->json(['message' => 'Pet not found'], 400);
        if ($pet->owner_id <> auth()->user()->id) return response()->json(['message' => 'No editing permissions'], 400);

        $pet->update(['status' => 4]);
        return response()->json(['success' => true, 'message' => 'Pet has been successfully removed']);
    }

    public function ripProfile(Request $request)
    {
        if(!$request->has('petId')) return response()->json(['message' => 'Pet not found'], 400);

        $pet = Pet::whereId($request->petId)->first();
        if (!$pet) return response()->json(['message' => 'Pet not found'], 400);
        if ($pet->owner_id <> auth()->user()->id) return response()->json(['message' => 'No editing permissions'], 400);

        if(!$request->has('death_date')) return response()->json(['message' => 'death_date not found'], 400);

        $date = Carbon::parse($request->death_date);

        $minDate = Carbon::createFromDate(1900, 1, 1);
        if (DateOperations::minDate($date, $minDate)) return response()->json(['message' => 'Minimum date 01.01.1900'], 400);
        $maxDate = Carbon::today()->addDay();
        if (DateOperations::maxDate($date, $maxDate)) return response()->json(['message' => 'Maximum date is today'], 400);

        $pet->update([
            'status' => 2,
            'death_date' => $date
        ]);
        return response()->json(['success' => true, 'message' => 'Pet successfully rip', 'pet' => Pet::format($pet)]);
    }

    public function autocomleteOwner(Request $request)
    {
        $splitName = explode(' ', $request->name, 2);
        $firstName = $splitName[0];
        $lastName = !empty($splitName[1]) ? $splitName[1] : ''; 
        $users = User::where('name', 'LIKE', $firstName."%")->where('surname', 'LIKE', $lastName."%")->limit(20)->get();
        $shortUsers = [];
        foreach($users as $user){
            $shortUsers[] = User::format($user, 'short');
        }
        return response()->json(['users' => $shortUsers]);
    }

    public function autocomletePet(Request $request)
    {
        $shortPets = [];
        $pets = Pet::whereOwnerId($request->userId)->whereSex($request->sex)->limit(20)->get();
        foreach($pets as $pet){
            $shortPets[] = Pet::format($pet, 'short');
        }
        return response()->json(['pets' => $shortPets]);
    }

    public function parentSend(Request $request)
    {
        $petId = $request->petId;
        if(!Pet::whereId($petId)->whereOwnerId(auth()->user()->id)->exists()) return response()->json(['message' => 'Pet not found'], 400);

        $parentId = $request->parentId;
        $parent = Pet::whereId($parentId)->first();
        if(!$parent) return response()->json(['message' => 'Pet parent not found'], 400);
        if($petId == $parentId) return response()->json(['message' => 'Pet == Parent!'], 400);

        if(Parents::wherePetId($petId)->whereType($parent->sex)->whereState(1)->exists()) return response()->json(['message' => 'The parent has already been added'], 400);
        if(Parents::wherePetId($petId)->whereParentId($parentId)->whereState(0)->exists()) return response()->json(['message' => 'The parent has already been sended'], 400);

        $state = ($parent->owner_id == auth()->user()->id) ? 1 : 0;

        Parents::create(['pet_id' => $petId, 'parent_id' => $parentId, 'type' => $parent->sex, 'state' => $state]);

        //Уведомление хозяину

        return response()->json(['message' => 'You have sent request parent']);
    }

    public function parentRequest(Request $request)
    {
        $requestId = $request->requestId;
        $req = Parents::whereId($requestId)->whereState(0)->first();
        if(!$req) return response()->json(['message' => 'Request not found'], 400);

        $parent = Pet::whereId($req->parent_id)->first();
        // $parent = Pet::whereId($req->parent_id)->whereOwnerId(auth()->user()->id)->first();
        if(!$parent) return response()->json(['message' => 'Pet parent not found'], 400);

        $state = ($request->type == 'accept') ? 1 : 2;

        $req->update(['state' => $state]);

        return response()->json(['message' => 'You have '.$request->type.' request parent']);
    }

    public function getParents($petId)
    {
        $parents = ['father' => null, 'mother' => null];
        $pets = Parents::wherePetId($petId)->whereState(1)->with('pet')->get();
        foreach ($pets as $pet) {
            $type = ($pet->pet->sex == 1) ? 'father' : 'mother';
            $parents[$type] = Pet::format($pet->pet, 'parent');
        }
        return response()->json(['parents' => $parents]);
    }

    public function getOffspring($petId)
    {
        $parents = [];
        $pets = Parents::whereParentId($petId)->whereState(1)->with('offsprings')->get();
        foreach ($pets as $pet) {
            $parents[] = Pet::format($pet->offsprings, 'parent');
        }
        return response()->json(['offsprings' => $parents, 'count' => $pets->count()]);
    }

    public function createAchievement(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'year' => 'required|digits:4|integer|min:1900|max:'.(date('Y')+1),
            'city' => 'required|string|min:2',
            'event' => 'required|string|between:2,255',
            'achievement' => 'required|string|between:2,255',
        ]);

        $city = City::whereNameRu($request->city)->first();
        if(!$city) $validator->errors()->add("city", "Enter valid city.");

        $petId = $request->petId;
        if(!Pet::whereId($petId)->whereOwnerId(auth()->user()->id)->exists()) $validator->errors()->add("petId", "Pet not found.");

        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }

        Achievement::create([
            'year' => $request->year,
            'event' => $request->event,
            'achievement' => $request->achievement,
            'pet_id' => $petId,
            'ID_City' => $city->id
        ]);

        return response()->json(['message' => 'Achievement successfully created']);
    }

    public function saveAchievement(Request $request) {
        $validator = Validator::make($request->all(), [
            'year' => 'required|digits:4|integer|min:1900|max:'.(date('Y')+1),
            'city' => 'required|string|min:2',
            'event' => 'required|string|between:2,255',
            'achievement' => 'required|string|between:2,255',
        ]);

        $city = City::whereNameRu($request->city)->first();
        if(!$city) $validator->errors()->add("city", "Enter valid city.");

        $achievementId = $request->achievementId;
        $achievement = Achievement::whereId($achievementId)->first();
        if(!$achievement) $validator->errors()->add("achievement", "Enter valid achievementId.");
        if(!Pet::whereId($achievement->pet_id)->whereOwnerId(auth()->user()->id)->exists()) $validator->errors()->add("petId", "Pet not found.");

        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }

        $achievement->update([
            'year' => $request->year,
            'event' => $request->event,
            'achievement' => $request->achievement,
            'ID_City' => $city->id
        ]);
        return response()->json(['message' => 'Achievement successfully edited']);
    }

    public function removeAchievement(Request $request) {
        if(!$request->has('achievementId')) return response()->json(['message' => 'achievementId not found'], 400);

        $achievementId = $request->achievementId;
        $achievement = Achievement::whereId($achievementId)->first();
        if (!$achievement) return response()->json(['message' => 'Enter valid achievementId'], 400);
        if (!Pet::whereId($achievement->pet_id)->whereOwnerId(auth()->user()->id)->exists()) return response()->json(['message' => 'No editing permissions'], 400);

        $achievement->delete();
        return response()->json(['message' => 'Achievement has been successfully removed']);
    }

    public function getAchievement($petId)
    {
        $achievements = Achievement::wherePetId($petId)->with('city')->get();
        return response()->json(['achievements' => $achievements->makeHidden(['ID_City', 'pet_id', 'id']), 'count' => $achievements->count()]);
    }

    public function getPetProfile($petId)
    {
        $pet = Pet::whereId($petId)->first();
        $petId = isset($pet->id) ? $pet->id : 0;

        return response()->json([
            'pet' => Pet::format($pet, 'pet_profile'),
            'parents' => self::getParents($petId)->original,
            'offsprings' => self::getOffspring($petId)->original,
            'achievements' => self::getAchievement($petId)->original
        ]);
    }

    public function search(Request $request)
    {
        $pets = \DB::table('pets')->where('pets.status', '<>', 4)->where('pets.name', 'like', $request->name.'%');
        if($request->has('sex') && $request->sex <> 2) $pets = $pets->whereSex($request->sex);

        $pets = $pets->join('users', 'pets.owner_id', '=', 'users.id');

        if($request->has('city')) {
            $city = explode(',', $request->city);
            if(count($city) == 1) {
                if($request->city <> '') $pets = $pets->where('users.ID_City', 0);
            }else{
                $country = Country::whereNameRu(trim($city[1]))->first();
                if(!$country) $pets = $pets->where('users.ID_City', 0);
                $cities = City::whereNameRu($city[0])->first();
                if(!$cities) $pets = $pets->where('users.ID_City', 0);
                $pets = $pets->where('users.ID_City', $cities->id);
            }


            // $ID_City = City::whereNameRu($request->city)->first();
            // if($ID_City) {
            //     $pets = $pets->where('users.ID_City', $ID_City->id);
            // }else{
            //     if($request->city <> '') $pets = $pets->where('users.ID_City', 0);
            // }
        }

        if($request->has('achievement')) {
            if($request->achievement == 1) $pets = $pets->join('achievements', 'pets.id', '=', 'achievements.pet_id');
        }

        $pets = $pets->select('pets.*')->paginate(10);
        $pets->getCollection()->transform(function ($value) {
            $pet = Pet::format($value, 'parent');
            return $pet;
        });

        return response()->json(['pets' => $pets]);
    }

}
