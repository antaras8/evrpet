<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Posts;
use App\Likes;
use App\User;
use App\Communities;
use App\Businesses;
use App\Pet;
use App\Comments;
use App\Friend;
use App\Participation;

class PostsController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'header' => 'required|string|between:1,2000',
            'blocks' => 'required|array',
            'object_id' => 'required|numeric',
            'type' => 'required|numeric'
        ]);

        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }

        $preview = ['images' => [], 'text' => ''];
        $paragraph = 0;
        foreach($request->blocks as $block){
            if($block['type'] == 'image'){
                // return dd($block);
                $preview['images'][] = $block['data']['url'];
            }
            if($block['type'] == 'paragraph' && $paragraph == 0){
                if(iconv_strlen($block['data']['text']) >= 480){
                    $preview['text'] = substr($block['data']['text'], 0, 480).'...';
                }else{
                    $preview['text'] = $block['data']['text'];
                }
                $paragraph = 1;
            }
        }

        $post = Posts::create([
            'author_id' => auth()->user()->id,
            'object_id' => $request->object_id,
            'header' => $request->header,
            'body' => json_encode($request->blocks),
            'type' => $request->type,
            'preview' => json_encode($preview),
            'draft' => 0
        ]);

        return response()->json(['success' => true, 'post_id' => $post->id]);
    }

    public function createComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'text' => 'required|string|between:1,300',
            'parents_id' => 'numeric',
            'object_id' => 'required|numeric'
        ]);

        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }

        $comment = Comments::create([
            'author_id' => auth()->user()->id,
            'object_id' => $request->object_id,
            'parents_id' => $request->parents_id,
            'text' => $request->text
        ]);

        return response()->json(['success' => true, 'comment_id' => $comment->id]);

    }

    public function removeComment(Request $request)
    {
        if(!$request->has('commentId')) return response()->json(['message' => 'Comment not found'], 400);

        $comment = Comments::whereId($request->commentId)->first();
        if (!$comment) return response()->json(['message' => 'Comment not found'], 400);
        if ($comment->author_id <> auth()->user()->id) return response()->json(['message' => 'No editing permissions'], 400);

        $comment->update([
            'text' => NULL
        ]);
        return response()->json(['success' => true, 'message' => 'Comment has been successfully removed']);
    }

    public function like(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
            'type' => 'required|numeric'
        ]);

        \DB::beginTransaction();

            if($request->type == 0){
                if(!Posts::whereDraft(0)->whereId($request->id)->exists()) $validator->errors()->add("id", "Post not found.");
            }else{
                if(!Comments::whereId($request->id)->exists()) $validator->errors()->add("id", "Comment not found.");
            }

            if(count($validator->errors()) > 0){
                return response()->json($validator->errors(), 400);
            }

            if($like = Likes::wherePostId($request->id)->whereType($request->type)->whereUserId(auth()->user()->id)->first()){
                $like->delete();
            }else{
                Likes::create([
                    'post_id' => $request->id,
                    'user_id' => auth()->user()->id,
                    'type' => $request->type
                ]);
            }

        \DB::commit();

        return response()->json(['success' => true]);
    }

    public function getComments($type, $id)
    {
        if($type == 'post'){
            $comments = Comments::whereObjectId($id)->whereParentsId(0)->with(['author'])->withCount(['likes' => function ($query) {$query->whereType(1);}, 'comments'])->paginate(10);
        }else{
            $comments = Comments::whereParentsId($id)->with(['author'])->withCount(['likes' => function ($query) {$query->whereType(1);}, 'comments'])->paginate(10);
        }

        foreach ($comments as $comment) {
            $comment->liked = Likes::wherePostId($comment->id)->whereType(1)->whereUserId(auth()->user()->id)->exists();
        }

        return response()->json(['comments' => $comments]);
    }

    public function getPost($post_id)
    {
        if(!$post = Posts::whereDraft(0)->whereId($post_id)->with(['author'])->select(['id', 'object_id', 'type', 'author_id', 'body', 'header', 'created_at'])->withCount(['likes' => function ($query) {$query->whereType(0);}, 'comments'])->first()) return response()->json(["success" => false, "error" => "Post not found."], 400);
        $post->liked = Likes::wherePostId($post->id)->whereType(0)->whereUserId(auth()->user()->id)->exists();
        if($post->type == 2){//club
            $post->object_name = Communities::whereId($post->object_id)->value('titleru') ?: 'Клуб';
        }elseif($post->type == 3){//busines
            $post->object_name = Businesses::whereId($post->object_id)->value('name') ?: 'Бизнес';
        }elseif($post->type == 4){//pet)
            $post->object_name = Pet::whereId($post->object_id)->value('name') ?: 'Питомец';
        }
        return response()->json(["success" => true, 'post' => $post]);
    }

    public function getPosts($type, $id)
    {
        switch ($type) {
            case 'user':
                if(!$object = User::whereId($id)->first()) return response()->json(["id" => "User not found."], 400);
                $type = 1;
                break;
            case 'club':
                if(!$object = Communities::whereId($id)->first()) return response()->json(["id" => "Club not found."], 400);
                $type = 2;
                break;
            case 'business':
                if(!$object = Businesses::whereId($id)->first()) return response()->json(["id" => "Business not found."], 400);
                $type = 3;
                break;
            case 'pet':
                if(!$object = Pet::whereId($id)->first()) return response()->json(["id" => "Pet not found."], 400);
                $type = 4;
                break;
            
            default:
            return response()->json(["type" => "Type not found."], 400);
                break;
        }
        $posts = Posts::whereDraft(0)->whereType($type)->whereObjectId($object->id)->with(['author'])->select(['id', 'object_id', 'type', 'author_id', 'preview', 'header', 'created_at'])->withCount(['likes' => function ($query) {$query->whereType(0);}, 'comments'])->orderByDesc('id')->paginate(5);

        foreach ($posts as $post) {
            $post->liked = Likes::wherePostId($post->id)->whereType(0)->whereUserId(auth()->user()->id)->exists();
        }

        return response()->json(['posts' => $posts]);
    }

    public function getFeed()
    {
        // $friends = array_merge(Friend::whereState(1)->whereUser1id(auth()->user()->id)->pluck('user2id')->toArray(), [auth()->user()->id]);
        // $posts = Posts::whereDraft(0)->whereType(1)->whereIn('object_id', $friends)->with(['author'])->select(['id', 'object_id', 'type', 'author_id', 'preview', 'header', 'created_at'])->withCount(['likes' => function ($query) {$query->whereType(0);}, 'comments'])->orderByDesc('id')->paginate(5);
        
        // foreach ($posts as $post) {
        //     $post->liked = Likes::wherePostId($post->id)->whereType(0)->whereUserId(auth()->user()->id)->exists();
        // }

        $participations = Participation::whereUserId(auth()->user()->id)->pluck('group_id');
        $friends = array_merge(Friend::whereState(1)->whereUser1id(auth()->user()->id)->pluck('user2id')->toArray(), [auth()->user()->id]);
        $posts = Posts::whereDraft(0)->whereType(1)->whereIn('object_id', $friends)->orWhere(['type' => 2])->whereIn('object_id', $participations)->with(['author'])->select(['id', 'object_id', 'type', 'author_id', 'preview', 'header', 'created_at'])->withCount(['likes' => function ($query) {$query->whereType(0);}, 'comments'])->orderByDesc('id')->paginate(5);
        
        foreach ($posts as $post) {
            $post->liked = Likes::wherePostId($post->id)->whereType(0)->whereUserId(auth()->user()->id)->exists();
        }
        
        return response()->json(['posts' => $posts]);
    }

    public function getFeedTest()
    {
        $participations = Participation::whereUserId(auth()->user()->id)->pluck('group_id');
        // $posts = Posts::whereDraft(0)->whereType(2)->whereIn('object_id', $participations)->with(['author'])->select(['id', 'object_id', 'type', 'author_id', 'preview', 'header', 'created_at'])->withCount(['likes' => function ($query) {$query->whereType(0);}, 'comments'])->orderByDesc('id')->paginate(5);
        $friends = array_merge(Friend::whereState(1)->whereUser1id(auth()->user()->id)->pluck('user2id')->toArray(), [auth()->user()->id]);
        $posts = Posts::whereDraft(0)->whereType(1)->whereIn('object_id', $friends)->orWhere(['type' => 2])->whereIn('object_id', $participations)->with(['author'])->select(['id', 'object_id', 'type', 'author_id', 'preview', 'header', 'created_at'])->withCount(['likes' => function ($query) {$query->whereType(0);}, 'comments'])->orderByDesc('id')->paginate(5);
        
        foreach ($posts as $post) {
            $post->liked = Likes::wherePostId($post->id)->whereType(0)->whereUserId(auth()->user()->id)->exists();
        }
        
        return response()->json(['posts' => $posts]);
    }

    public function remove(Request $request)
    {
        if(!$request->has('postId')) return response()->json(['message' => 'Post not found'], 400);

        $post = Posts::whereId($request->postId)->first();
        if (!$post) return response()->json(['message' => 'Post not found'], 400);
        if ($post->author_id <> auth()->user()->id) return response()->json(['message' => 'No editing permissions'], 400);

        $post->delete();
        return response()->json(['success' => true, 'message' => 'Post has been successfully removed']);
    }

    public function edit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'header' => 'required|string|between:1,2000',
            'blocks' => 'required|array'
        ]);

        if(!$request->has('postId')) $validator->errors()->add("postId", "Post not found");

        $post = Posts::whereId($request->postId)->first();
        if (!$post) $validator->errors()->add("postId", "Post not found");
        if ($post->author_id <> auth()->user()->id) $validator->errors()->add("postId", "No editing permissions");

        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }

        $preview = ['images' => [], 'text' => ''];
        $paragraph = 0;
        foreach($request->blocks as $block){
            if($block['type'] == 'image'){
                // return dd($block);
                $preview['images'][] = $block['data']['url'];
            }
            if($block['type'] == 'paragraph' && $paragraph == 0){
                if(iconv_strlen($block['data']['text']) >= 480){
                    $preview['text'] = substr($block['data']['text'], 0, 480).'...';
                }else{
                    $preview['text'] = $block['data']['text'];
                }
                $paragraph = 1;
            }
        }

        $post->update([
            'header' => $request->header,
            'body' => json_encode($request->blocks),
            'preview' => json_encode($preview)
        ]);

        return response()->json(['success' => true, 'message' => 'Post has been successfully edited', 'post_id' => $post->id]);
    }
}