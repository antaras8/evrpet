<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Friend;
use App\User;
use App\City;

class FriendsController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function add(Request $request)
    {
        $friendId = $request->friendId;
        if(!User::whereId($friendId)->exists()) return response()->json(['message' => 'User not found'], 400);

        $viewerId = auth()->user()->id;
        if($friendId == $viewerId) return response()->json(['message' => 'You can\'t add yourself as a friend'], 400);

        $value = Friend::whereUser2id($viewerId)->whereUser1id($friendId)->first();
        if($value){
            if($value->state == 1){
                return response()->json(['message' => 'Friend has already been added'], 400);
            }else{
                $value->delete();
                return response()->json(['success' => true, 'message' => 'Request to friend has been successfully cancelled', 'type' => 'cancel']);
            }
        }else{
            Friend::create(['user2id' => $viewerId, 'user1id' => $friendId]);
            return response()->json(['success' => true, 'message' => 'Request to friend has been successfully sent', 'type' => 'request']);
        }
    }

    public function process(Request $request)
    {
        $friendId = $request->friendId;
        $viewerId = auth()->user()->id;
        $value = Friend::whereUser1id($viewerId)->whereUser2id($friendId)->first();
        if(!$value) return response()->json(['message' => 'Friend request not found'], 400);
        if($value->state <> 0) return response()->json(['message' => 'Friend request has already been processed'], 400);

        if($request->type == 'accept'){
            Friend::create(['user1id' => $friendId, 'user2id' => $viewerId, 'state' => 1]);
            $value->update(['state' => 1]);
            return response()->json(['success' => true, 'message' => 'Friend successfully added']);
        }else{
            $value->delete();
            return response()->json(['success' => true, 'message' => 'Friend request successfully canceled']);
        }
    }

    public function remove(Request $request)
    {
        $viewerId = auth()->user()->id;
        $friendId = $request->friendId;

        $value = Friend::whereState(1)->whereUser1id($viewerId)->whereUser2id($friendId)->first();
        if(!$value){
            return response()->json(['message' => 'Friend not found'], 400);
        }else{
            Friend::whereId($value->id)->delete();
            Friend::whereState(1)->whereUser2id($viewerId)->whereUser1id($friendId)->delete();
            return response()->json(['message' => 'Friend successfully removed']);
        }
    }

    static public function getUserFriends($thisid) {
        $viewerId = auth()->user()->id;
        $requests = ($thisid == $viewerId) ? Friend::whereState(0)->whereUser1id($thisid)->with('user')->get()->makeHidden(['user1id', 'user2id', 'id']) : null;
        $friends = Friend::whereState(1)->whereUser1id($thisid)->with('user')->paginate(9);
        $friendsLists = $friends->pluck('user2id');
        $viewerFriends = Friend::whereUser2id($viewerId)->whereIn('user1id', $friendsLists)->get();
        // return $viewerFriends;
        foreach ($friends as $key => $friend) {
            // $friend->checkFriend = $viewerFriends->where('user1id', $friend->user2id)->first();
            $checkFriend = $viewerFriends->where('user1id', $friend->user2id)->first();
            // dd($checkFriend);
            if($viewerId == $friend->user->id){
                $friend->state = 0;
            }elseif(!$checkFriend){
                $friend->state = -1;
            }elseif($checkFriend->state == 0){
                $friend->state = 1;
            }elseif($checkFriend->state == 1){
                $friend->state = 2;
            }
            // $friend->state = (!$checkFriend) ? -1 : 2;
            // $friend->user->city = City::whereId($friend->user->ID_City)->value('name_ru');
        }
        return response()->json(['friends' => $friends->makeHidden(['user1id', 'user2id', 'id']), 'requests' => $requests, 'count' => Friend::whereState(1)->whereUser1id($thisid)->count()]);
    }

    // static public function getUserFriends($thisid) {
    //     $viewerId = auth()->user()->id;
    //     $requests = ($thisid == $viewerId) ? Friend::whereState(0)->whereUser1id($thisid)->with('user')->get()->makeHidden(['user1id', 'user2id', 'id']) : null;
    //     $friends = Friend::whereState(1)->whereUser1id($thisid)->with('user')->paginate(9);
    //     $friendsLists = $friends->pluck('user2id');
    //     $viewerFriends = Friend::whereState(1)->whereUser1id($viewerId)->whereIn('user2id', $friendsLists)->get();
    //     foreach ($friends as $key => $friend) {
    //         $checkFriend =  $viewerFriends->where('user2id', $friend->user2id)->toArray();
    //         $friend->state = (!$checkFriend) ? -1 : 2;
    //         $friend->user->city = City::whereId($friend->user->ID_City)->value('name_ru');
    //     }
    //     return response()->json(['friends' => $friends->makeHidden(['user1id', 'user2id', 'id']), 'requests' => $requests]);
    // }

    static public function getUserFriendsMutual($thisid) {
        $viewerId = auth()->user()->id;
        $friends = Friend::whereState(1)->whereUser1id($thisid)->with('user')->paginate(6);
        foreach ($friends as $key => $friend) {
            $friend->mutual = Friend::whereState(1)->whereUser1id($viewerId)->whereUser2id($friend->user2id)->count();
            if($thisid == $viewerId) $friend->mutual -= 1;
        }
        return response()->json(['friends' => $friends->makeHidden(['user1id', 'user2id', 'id']), 'count' => Friend::whereState(1)->whereUser1id($thisid)->count()]);
    }

    

    // static public function getUserFriends($thisid) {
    //     $friendsResult = [];
    //     $friendsResult['friends'] = [];
    //     $friendsResult['requests'] = [];
    //     $couples = Friend::where('user1id', $thisid)->orWhere('user2id', $thisid)->get();
    //     foreach ($couples as $couple) {
    //         $otherid = $couple->user1id == $thisid ? $couple->user2id : $couple->user1id;
    //         if($couple->state == 1){
    //             $friendsResult['friends'] [] = User::format(User::whereId($otherid)->first(), 'friends');
    //         }else{
    //             $friendsResult['requests'] [] = User::format(User::whereId($otherid)->first(), 'friends');
    //         }
    //     }

    //     $count = count($friendsResult['friends']);

    //     $friendsResult = self::format($friendsResult, auth()->user()->id, $thisid);
    //     return response()->json(['friends' => $friendsResult, 'count' => $count]);
    // }

    // public function add(Request $request)
    // {
    //     $friendId = $request->friendId;
    //     if(!User::whereId($friendId)->exists()) return response()->json(['message' => 'User not found'], 400);

    //     $viewerId = auth()->user()->id;
    //     if($friendId == $viewerId) return response()->json(['message' => 'You can\'t add yourself as a friend'], 400);
    //     $state = Friend::isFriend($viewerId, $friendId);
    //     if ($state == 0) {
    //         Friend::create(['user1id' => $viewerId, 'user2id' => $friendId]);
    //         return response()->json(['message' => 'Request to friends has been successfully sent']);
    //     }else{
    //         $message = ($state == 1) ? 'Friend has already been added' : 'Friend request has already been sent';
    //         return response()->json(['message' => $message], 400);
    //     }
    // }

    // public function process(Request $request)
    // {
    //     $friendId = $request->friendId;
    //     $viewerId = auth()->user()->id;
    //     $reqFriend = Friend::whereUser2id($viewerId)->whereUser1id($friendId)->first();
    //     if(!$reqFriend) return response()->json(['message' => 'Friend request not found'], 400);
    //     if($reqFriend->state <> 0) return response()->json(['message' => 'Friend request has already been processed'], 400);

    //     if($request->type == 'accept'){
    //         $reqFriend->update(['state' => 1]);
    //         return response()->json(['message' => 'Friend successfully added']);
    //     }else{
    //         $reqFriend->delete();
    //         return response()->json(['message' => 'Friend request successfully canceled']);
    //     }
    // }

    // public function remove(Request $request)
    // {
    //     $viewerId = auth()->user()->id;
    //     $friendId = $request->friendId;
    //     $state = Friend::isFriend($viewerId, $friendId, 1);
    //     if($state == 0){
    //         return response()->json(['message' => 'Friend not found'], 400);
    //     }else{
    //         Friend::whereId($state->id)->delete();
    //         return response()->json(['message' => 'Friend successfully removed']);
    //     }
    // }

    // static function format($guests, $viewerId, $thisid) {
    //     if($viewerId == $thisid){
    //         foreach ($guests['friends'] as $item) {
    //             $item->friend = 2;
    //             // $item->mutual_friends = Friend::where('user1id', $item->thisid)->orWhere('user2id', $item->thisid)->where('state', 1)->get();
    //             // $item->matual_friends = Friend::where('user1id', $item->thisid)->orWhere('user2id', $item->thisid)->where('user1id', $thisid)->orwhere('user2id', $thisid)->get();
    //         }
    //         foreach ($guests['requests'] as $item) {
    //             $item->friend = 1;
    //         }
    //         return array_merge($guests['requests'], $guests['friends']);
    //     }else{
    //         foreach ($guests['friends'] as $item) {
    //             $item->friend = Friend::isFriend($viewerId, $item->thisid, 0);
    //         }
    //         return $guests['friends'];
    //     }
    // }
}
