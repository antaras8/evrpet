<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\City;
use App\Country;
use App\Pet;
use App\Blacklist;
use App\Friend;
use Validator;
use App\Helpers\DateOperations;
use Carbon\Carbon;

class UsersController extends Controller
{

    public function __construct() {
        $this->middleware('auth:api');
    }

    public function removeProfile()
    {
        User::whereId(auth()->user()->id)->update(['status' => -1]);
        auth()->logout();
        return response()->json(['success' => true, 'message' => 'User has been successfully removed']);
    }

    public function saveProfile(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,50',
            'surname' => 'required|string|between:2,50',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'city' => 'required|string|min:2',
            'title' => 'nullable|string|between:1,255',
        ]);

        if (preg_match_all('/^[a-zа-йА-ЯёйЁЙьЬъЪыЫуУшШщЩцЦхХфФэЭжЖрРюЮчЧтТсСяЯ]+$/mi', $request->name) == 0) $validator->errors()->add("name", "Имя может содержать только буквы.");
        if (preg_match_all('/^[a-zа-йА-ЯёйЁЙьЬъЪыЫуУшШщЩцЦхХфФэЭжЖрРюЮчЧтТсСяЯ]+$/mi', $request->surname) == 0) $validator->errors()->add("surname", "Фамилия может содержать только буквы.");

        $city = explode(',', $request->city);
        if(count($city) == 1) {
            $validator->errors()->add("city", "Страна не введена");
        }else{
            $country = Country::whereNameRu(trim($city[1]))->first();
            if(!$country) $validator->errors()->add("city", "Страна не найдена");
            $cities = City::whereNameRu($city[0])->first();
            if(!$cities) $validator->errors()->add("city", "Город не найден.");
        }

        if($request->has('birth_time')){
            $date = Carbon::parse($request->birth_time);
            $minDate = Carbon::createFromDate(1900, 1, 1);
            if (DateOperations::minDate($date, $minDate)) $validator->errors()->add("specie", "Minimum date 01.01.1900.");
            $maxDate = Carbon::today()->addDay();
            if (DateOperations::maxDate($date, $maxDate)) $validator->errors()->add("specie", "Maximum date is today.");
        }

        if(count($validator->errors()) > 0){
            return response()->json($validator->errors(), 400);
        }

        User::whereId(auth()->user()->id)->update([
            'name' => $request->name,
            'surname' => $request->surname,
            'title' => $request->title,
            'phone' => $request->phone,
            'ID_City' => $cities->id,
            'birth_time' => ($request->has('birth_time')) ? $date : auth()->user()->birth_time
        ]);
        return response()->json(['success' => true, 'message' => 'User successfully edited', 'user' => User::format(User::find(auth()->user()->id), 'profile')]);
    }

    public function removeAvatar()
    {
        User::whereId(auth()->user()->id)->update([
            'picture' => "/storage/images/avatars/no-photo.png"
        ]);

        return response()->json(['success' => true, 'message' => 'Avatar removed', 'avatar' => "/storage/images/avatars/no-photo.png"]);
    }

    public function blacklistAdd(Request $request)
    {
        $userId = $request->userId;
        if(!User::whereId($userId)->exists()) return response()->json(['message' => 'User not found'], 400);

        $viewerId = auth()->user()->id;
        if($userId == $viewerId) return response()->json(['message' => 'You can\'t block yourself'], 400);

        $type = $request->type;
        if(!in_array($type, ['profile', 'message'])) return response()->json(['message' => 'Type not found'], 400);

        if (!Blacklist::inBlacklist($userId, $viewerId, $type)) {
            if($type == 'profile'){
                Friend::whereUser1id($userId)->whereUser2id($viewerId)->delete();
                Friend::whereUser1id($viewerId)->whereUser2id($userId)->delete();
            }

            Blacklist::create(['user1id' => $viewerId, 'user2id' => $userId, 'type' => $type]);
            return response()->json(['message' => 'You have blocked the user']);
        }else{
            return response()->json(['message' => 'You have already blocked the user'], 400);
        }
    }

    public function blacklistRemove(Request $request)
    {
        $userId = $request->userId;
        if(!User::whereId($userId)->exists()) return response()->json(['message' => 'User not found'], 400);

        $viewerId = auth()->user()->id;
        if($userId == $viewerId) return response()->json(['message' => 'You can\'t unblock yourself'], 400);

        $type = $request->type;
        if(!in_array($type, ['profile', 'message'])) return response()->json(['message' => 'Type not found'], 400);

        if (Blacklist::inBlacklist($userId, $viewerId, $type)) {
            Blacklist::whereUser1id($viewerId)->whereUser2id($userId)->whereType($type)->delete();
            return response()->json(['message' => 'You have unblocked the user']);
        }else{
            return response()->json(['message' => 'You have already unblocked the user'], 400);
        }
    }

    public function getPetsUser($userId)
    {
        $shortPets = [];
        $pets = Pet::whereOwnerId($userId)->where('status', '<>', 4)->orderBy('id', 'desc')->get();
        foreach($pets as $pet){
            $shortPets[] = Pet::format($pet, 'user_profile');
        }
        return response()->json(['pets' => $shortPets, 'count' => $pets->count()]);
    }

    public function getUserProfile($slug)
    {
        if(is_numeric($slug)){
            $userId = (int)$slug;
        }else{
            if (strpos($slug, 'user') !== false) {
                $userId = intval(preg_replace('/[^0-9]+/', '', $slug), 10);
            }else{
                $userId = User::whereSlug($slug)->value('id');
            }
        }

        $user = User::whereId($userId)->first();
        $userId = isset($user->id) ? $user->id : 0;
        $friendState = Friend::whereUser1id($userId)->whereUser2id(auth()->user()->id)->value('state');

        $block = (int)Blacklist::isBlacklist($userId, auth()->user()->id, 'profile');
        
        if($friendState !== null){
            $friendState = ($friendState == 0) ? 1 : 2;
        }else{
            $friendState = -1;
        }
        if($userId == auth()->user()->id) $friendState = 0;

        return response()->json([
            'user' => User::format($user, 'profile'),
            'block' => $block,
            'pets' => self::getPetsUser($userId)->original['pets'],
            'friend' => $friendState,
            'friends' => FriendsController::getUserFriendsMutual($userId)->original
        ]);
    }

    public function uploadAvatar(Request $request)
    {
        // $request->validate([
        //     'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        // ]);

        $file = $request->file;
        $file = str_replace('data:image/png;base64,', '', $file);
        $file = str_replace(' ', '+', $file);

        $picture = UploadController::save(base64_decode($file), 'avatars', auth()->user()->picture); 

        // $picture = UploadController::save($request->file, 'avatars', auth()->user()->picture); 

        User::whereId(auth()->user()->id)->update([
            'picture' => $picture,
        ]);

        return response()->json(['avatar' => $picture, 'success' => true]);
    }

    public function search(Request $request)
    {
        $users = \DB::table('users')->where('users.status', '<>', -1)->where('users.name', 'like', $request->name.'%')->where('users.surname', 'like', $request->surname.'%');

        if($request->has('location')) {
            if($request->location == 1) $users = $users->where('users.ID_City', auth()->user()->ID_City);
        }

        if($request->has('people')) {
            if($request->people == 1) {
                $users = $users->join('friends', 'users.id', '=', 'friends.user1id')->where('friends.user2id', auth()->user()->id);
            }
        }

        $users = $users->select('users.id', 'users.name', 'users.surname', 'users.slug', 'users.picture', 'users.ID_City')->paginate(10);

        $lists = $users->pluck('id');
        $viewerId = auth()->user()->id;
        $viewerFriends = Friend::whereUser2id($viewerId)->whereIn('user1id', $lists)->get();

        $users->getCollection()->transform(function ($value) use ($viewerFriends, $viewerId) {
            $checkFriend = $viewerFriends->where('user1id', $value->id)->first();

            if($viewerId == $value->id){
                $value->state = 0;
            }elseif(!$checkFriend){
                $value->state = -1;
            }elseif($checkFriend->state == 0){
                $value->state = 1;
            }elseif($checkFriend->state == 1){
                $value->state = 2;
            }
            $user = [
                'thisid' => $value->id,
                'fullname' => $value->name.' '.$value->surname,
                'url' => ($value->slug <> NULL) ? $value->slug : "user{$value->id}",
                'avatar' => $value->picture,
                'city' => City::whereId($value->ID_City)->value('name_ru'),
                'state' => $value->state
            ];
            return $user;
        });

        return response()->json(['users' => $users]);
    }
}
