<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class UploadController extends Controller
{
    static public function save($file, $dir, $default)
    {
        if ($file) {
            $ImageUpload = Image::make($file);
            $originalPath = storage_path('app/public').'/images/'.$dir.'/';
            $name = Str::random(10).time().'.png';
            $ImageUpload->save($originalPath.$name);
            
            // $ImageUpload->resize(250,125);
            // $ImageUpload = $ImageUpload->save($originalPath.time().'_250x125'.'.png');
            return '/storage/images/'.$dir.'/'.$name;
        }
        
        return $default;
        
    }

}
