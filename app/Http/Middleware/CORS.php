<?php
namespace App\Http\Middleware;
use Illuminate\Http\Request;
use Closure;
class CORS {
    
    public function handle(Request $request, Closure $next) {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST');
        header('Access-Control-Allow-Headers: Authorization, Kek');
        return $next($request);
    }
}