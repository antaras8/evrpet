<?php

namespace App\Helpers;

use Carbon\Carbon;
use App\Helpers\YearsDeclensions;

	class Age
	{
		static function getAgeByBirthtime($birthtime) {
			if($birthtime == NULL) return NULL;
			$age = Carbon::parse($birthtime)->diff(Carbon::now())->y;
			if ($age < 1){
				$month = Carbon::parse($birthtime)->diff(Carbon::now())->m;
				return $month.' '.YearsDeclensions::getMonth($month);
			} else {
				return $age.' '.YearsDeclensions::getAge($age);
			}
		}
	}