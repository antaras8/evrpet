<?php

namespace App\Helpers;

use Carbon\Carbon;

	class DateOperations
	{
		static function minDate($date, $minDate)
		{
			return Carbon::parse($minDate)->greaterThan($date);
		}
	
		static function maxDate($date, $maxDate)
		{
			return !Carbon::parse($maxDate)->greaterThan($date);
		}
	}