<?php

namespace App\Helpers;

	class YearsDeclensions
	{
		static function getAge($count) {
			if ($count >= 11 && $count <= 19)
				$count = 0;
			$count = $count % 10;
			if ($count == 1) return "год";
			if ($count >= 2 && $count <= 4) return "года";
			if ($count == 0) return "лет";
			if ($count >= 5) return "лет";
		}

		static function getMonth($count) {
			if ($count >= 11 && $count <= 19)
				$count = 0;
			$count = $count % 10;
			if ($count == 1) return "месяц";
			if ($count >= 2 && $count <= 4) return "месяца";
			if ($count == 0) return "месяцев";
			if ($count >= 5) return "месяцев";
		}
	}
