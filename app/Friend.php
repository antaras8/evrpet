<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Friend extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user1id', 'user2id', 'state'
    ];

    /*
    state 
    -1 - не друг
    0 - аккаунт визетера
    1 - не друг / заявка отправлена
    2 - друг

    */

    const UPDATED_AT = NULL;
    const CREATED_AT = NULL;

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user2id')->select(['id', 'name', 'surname', 'picture', 'ID_City'])->with('city');
    }

    // static public function isFriend($viewerid, $friendId, $data = 0) {
    //     $exist = \DB::select('select * from friends where (user1id = '.$viewerid.' and user2id = '.$friendId.') or (user2id = '.$viewerid.' and user1id = '.$friendId.') limit 1');
    //     if($exist){
    //         if($data == 1){
    //             $state = $exist[0];
    //         }else{
    //             $state = ($exist[0]->state == 0) ? 1 : 2;
    //         }
    //     }else{
    //         $state = 0;
    //     }
    //     return $state;
    // }

    // static function checkFriend($viewerid, $friendId)
    // {
    //     $value = $this->whereUser1id($viewerid)->whereUser2id($friendId)->first();
    // }
}
