<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Communitie extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'group', 'titleru'
    ];

    const UPDATED_AT = NULL;
    const CREATED_AT = NULL;

    public function participation()
    {
        return $this->hasMany('App\Participation', 'group_id', 'id');
    }

    static function format($item, $type = null)
    {
        if (!$item) return;

        if($type == 'short'){
            
        }

        $item->url = "community{$item->id}";
        $item->cover = "/storage/images/communities/cover/".$item->id.".png";
        return $item;
    }
    
    // static function find()
    // {
    //     $group = $_POST["group"];
    //     if ($group) 
    //         $items = database::queryAll("select * from communities where `group` = ? limit 40", [$group]);
    //     else
    //         $items = database::queryAll("select * from communities where `group` <> '' limit 40", []);
    //     return self::format($items);
    // }
    
    
    // static function getOne($thisid)
    // {
    //     $item = database::queryOne("select * from communities where thisid = ? limit 1", [$thisid]);
    //     return self::format($item);
    // }
    
    // static function findOneByCode($code)
    // {
    //     $item = database::queryOne("select * from communities where code = ? limit 1", [$code]);
    //     return self::format($item);
    // }
    
    // static function getGroupPicture($code)
    // {
    //     return "~/img/pet-$code.svg";
    // }
        
    // static function autocomplete()
    // {
    //     $findGroup = $_GET["group"];
    //     $findInput = trim($_GET["query"], "?");
    //     $items = database::queryAll("select * from communities where `group` = ? limit 40", [$findGroup]);
    //     self::format($items);
    //     foreach ($items as $index => $val) {
    //         $val->text = $val->title;
    //     }
    //     return $items;
    // }
}
