<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dialog extends Model
{
    public $fillable = ['first_id', 'second_id', 'last_message_id', 'last_sender_id', 'unread'];

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;

    public function lastMessage()
    {
        return $this->hasOne('App\Message', 'id', 'last_message_id')->with('sender')->select(['text', 'created_at', 'sender_id', 'id']);
    }
}
