<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Communitie;

class Participation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'group_id'
    ];

    const UPDATED_AT = NULL;
    const CREATED_AT = NULL;

    static function isSubscribed($groupId, $userId)
    {
        return self::whereUserId($userId)->whereGroupId($groupId)->exists();
    }
    
    static function subscribe($userId, $groupId)
    {
        if (!Communitie::whereId($groupId)->exists()) return;
        if (self::isSubscribed($groupId, $userId)) return;
        self::create(['user_id' => $userId, 'group_id' => $groupId]);
    }
    
    static function unsubscribe($userId, $groupId)
    {
        $item = self::whereUserId($userId)->whereGroupId($groupId)->first();
        if ($item) $item->delete();
    }
}
